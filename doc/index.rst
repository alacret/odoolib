.. odoolib documentation master file, created by
   sphinx-quickstart on Tue Aug 26 14:07:40 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Welcome to odoolib's documentation!
===================================

Contents:

.. toctree::
   :maxdepth: 2

   modules.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. automodule:: odoolib
