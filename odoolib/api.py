# -*- coding: UTF-8 -*-
"""
.. moduleauthor:: Juan Hernandez <vladjanicek@gmail.com> @vladjanicek

"""

import base64
import xmlrpclib
import time
import types


class _OdooModel(object):
    def __init__(self, odoolib, model):
        self._odoolib = odoolib
        self._model_name = model

    def _set_records(self, vals, operator='='):
        """ Transforms kwargs into odoo query format

        :param vals: Odoo Vals got from a kwargs
        :return: odoo query format

        """
        return [(key, operator, value) for key, value in vals.iteritems()]

    def add(self, *args, **kwargs):
        records = kwargs or args[0]
        return self._odoolib.q_add(self._model_name, records)

    def all(self, model_fields=False):
        return self.get(model_fields=model_fields)

    def get(self, operator='=', offset=False, limit=False, order=False,
            model_fields=False, **kwargs):
        ids = []
        if kwargs:
            ids = self._set_records(kwargs, operator=operator)
        res = self._odoolib.q_browse(self._model_name, ids,
                                     offset=offset, limit=limit,
                                     order=order, model_fields=model_fields)
        # Consistenly return the same data type
        # if len(res) == 1:
        #     res = res[0]
        return res

    def search(self, arguments=[], offset=False, limit=False, order=False,
            model_fields=False):
        """ Search consistenly with the arguments passed
        :param arguments: a list of tuples in the format  [(model_field,operator,Value)]
        Where operators can be: ilike, =, !=, >=, <=
        :return: result set
        TODO: Defensive check of arguments
        """
        res = self._odoolib.q_browse(self._model_name, arguments,
                                     offset=offset, limit=limit,
                                     order=order, model_fields=model_fields)
        return res

    def exec_workflow(self, wf_name, record_id):
        return self._odoolib.q_workflow(self._model_name, wf_name, record_id)

    def get_report(self, report_model, model_id, filename=''):
        return self._odoolib.generate_report(self._model_name,
                                             report_model,
                                             model_id,
                                             filename)

    def update(self, update_id, *args, **kwargs):
        records = kwargs or args[0]
        return self._odoolib.q_update(self._model_name, update_id, records)

    def delete(self, ids):
        if isinstance(ids, types.IntType):
            ids = [ids]
        return self._odoolib.q_delete(self._model_name, ids)


class OdooLib(object):
    """Odoo/OpenERP XML-RPC Easy AD-HOC webservices access

    Will raise exception if user/password is/are not correct

    """

    def __init__(self, url, db, user, passwd):
        self.url = url
        self.db = db
        self.user = user
        self.passwd = passwd

        self.uid = self.login_openerp()

        if not self.uid:
            raise Exception('Incorrect User/Password')

        ## Create basic browse access to models
        models = self.q_browse('ir.model')
        self.odoo = {}

        for model in models:
            model_name = model.get('model').replace('.', '_')
            self.__setattr__(model_name, _OdooModel(self, model.get('model')))

    def login_openerp(self):
        """Login via XML-RPC

        Returns user_id and handles it's exceptions thru the console and

        """
        conn = xmlrpclib.ServerProxy(self.url + '/xmlrpc/common')
        uid = False
        try:
            uid = conn.login(self.db, self.user, self.passwd)
        except Exception, e:
            raise e
        return uid

    def q_add(self, model, records):
        """Insert query

        :param model: openerp model
        :param records: records to add (dictionary)

        """

        db_conn = xmlrpclib.ServerProxy(self.url + '/xmlrpc/object')
        return db_conn.execute(self.db, self.uid, self.passwd,
                               model, 'create', records)

    def q_search(self, model, q, offset=False, limit=False, order=False):
        """Search query, returns IDs

        :param model: openerp model
        :param q: query to perform filtering

        """

        db_conn = xmlrpclib.ServerProxy(self.url + '/xmlrpc/object')
        return db_conn.execute(self.db, self.uid, self.passwd,
                               model, 'search', q, offset, limit, order)

    def q_read(self, model, ids, model_fields=[]):
        """Read query, returns info from given IDs

        :param model: openerp model
        :param ids: Record IDs

        """

        db_conn = xmlrpclib.ServerProxy(self.url + '/xmlrpc/object')
        return db_conn.execute(self.db, self.uid, self.passwd, model, 'read',
                               ids, model_fields)

    def q_browse(self, model, q=[], offset=False, limit=False,
                 order=False, model_fields=[]):
        """Browse Records

        :param model: openerp model
        :param q: query to perform filtering

        """
        ids = self.q_search(model, q, offset=offset, limit=limit, order=order)
        return self.q_read(model, ids, model_fields=model_fields)

    def q_update(self, model, update_id, r):
        db_conn = xmlrpclib.ServerProxy(self.url + '/xmlrpc/object')
        return db_conn.execute(self.db, self.uid, self.passwd,
                               model, 'write', update_id, r)

    def q_workflow(self, model, wf_name, record_id):
        db_conn = xmlrpclib.ServerProxy(self.url + '/xmlrpc/object')
        db_conn.exec_workflow(self.db, self.uid, self.passwd, model,
                              wf_name, record_id)

    def q_delete(self, model, ids):
        """Deletes records

        @params ids: list of ids to delete

        """
        db_conn = xmlrpclib.ServerProxy(self.url + '/xmlrpc/object')
        return db_conn.execute(self.db, self.uid, self.passwd,
                               model, 'unlink', ids)

    def generate_report(self, model, report_model, doc_id, filename=''):
        """Get Odoo Report files

        :param model: OpenERP Model
        :param report_model: Report Model
        :param doc_id: Document ID
        :return file: File object

        """
        print_sock = xmlrpclib.ServerProxy(self.url + '/xmlrpc/report')
        id_report = print_sock.report(self.db,
                                      self.uid,
                                      self.passwd,
                                      report_model,
                                      [doc_id], {'model': model,
                                                 'id': doc_id,
                                                 'report_type': 'pdf'},
                                      {'lang': u'en_US',
                                       'client': 'web',
                                       'tz': u''})
        report = {}
        if id_report:
            state = False
            attempt = 0
            while not state:
                try:
                    report = print_sock.report_get(self.db, self.uid,
                                                   self.passwd, id_report)

                except Exception, e:
                    raise Exception('\nException: {}'.format(e))

                state = report['state']
                if not state:
                    time.sleep(1)
                    attempt += 1
                if attempt > 200:
                    raise xmlrpclib.error.RPCError("Download time exceeded, "
                                                   "the operation has been "
                                                   "canceled.")

            if report.get('result'):
                string_pdf = base64.decodestring(report['result'])
                if filename:
                    file_pdf = open(filename, 'w')
                    file_pdf.write(string_pdf)
                    file_pdf.close()
                else:
                    return string_pdf

        return True
